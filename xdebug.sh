#!/usr/bin/env bash
source .utils

case $1 in

  on)
    echo -e $Yellow"Turning on XDebug and recreating the container..."$Noc;
    .env -f php-apache/.env set PHP_EXTENSION_XDEBUG=1;
    ./recreate-apache.sh;

    ;;

  off)
    echo -e $Yellow"Disabling XDebug and recreating the container..."$Noc;
    .env -f php-apache/.env set PHP_EXTENSION_XDEBUG=0;
    ./recreate-apache.sh;
    ;;

  *)
    echo -e $Red"You have not specified either on or off as an argument";
    exit 1;
    ;;
esac