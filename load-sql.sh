#!/usr/bin/env bash
source .utils
CONTAINER=shared-mysql
clear
echo -e $Yellow"======================================================================================="
echo -e        "   DANGER ZONE! DESTRUCTIVE OPERATION! PLEASE MAKE SURE YOU KNOW WHAT YOU ARE DOING    "
echo -e        "======================================================================================="$Noc
echo -e $Red"Drop a DATABASE"$Noc" and load the SQL specified next in it?"$Noc
read -p "(y/n)? " yn
    case $yn in
        [Yy]* )
            echo -e $Green"Please specify the name of the SQL file in the current folder or the full path to it."$Noc;
            echo -n "SQL file name or path: " 
            read FILE
                if [ -f "$FILE" ]; then
                    echo -e $Green"File was found."$Noc;
                else 
                    echo -e $Red"$FILE does not exist. Please try again with the proper name and or full path..."$Noc;
                    exit 
                fi;
            echo -e "Which database to "$Red"DROP?"$Noc;
            read -p $(echo -e $Yellow"Database>"$Red) DATABASE
            clear
            echo -e $Green"Got it. Here is what we are going to do:\n"
            echo -e $Yellow"======================================================================================="
            echo -e         "   - "$Red"DROP the database $DATABASE"$Yellow" and recreate it"
            echo -e         "   - "$Green"RUN the file $FILE"
            echo -e         "     into the blank database $DATABASE"$Yellow
            echo -e         "======================================================================================="$Noc
            echo -e $Noc"Are you "$Yellow"absolutely sure"$Noc" of what you're doing? \nIf so, press enter "$Red"to DROP and run SQL into "$Yellow"$DATABASE"$Noc". Press ctrl+c to cancel."
            read cancel
            clear 
            echo -e $Green"Dropping and recreating the database "$BYellow"$DATABASE,"$Green" if exists... Container info:"$Noc
            docker exec $CONTAINER mysql -u root --password=root -e "DROP DATABASE IF EXISTS $DATABASE; CREATE DATABASE $DATABASE";
            echo -e $Green"Executing: $FILE \ninto the MySQL database "$BYellow"$DATABASE"$Green"... Container info:"$Noc
            cat $FILE | docker exec -i $CONTAINER /usr/bin/mysql -u root --password=root $DATABASE;
            echo "======================================================================================="
            echo -e $Yellow"    Operations have ended."$Noc
            echo "======================================================================================="
            echo -e "MySQL might issue warnings depending on its config. \nIf no errors were shown, your database should now be populated with the SQL provided."
        ;;
        [Nn]* )
            echo "No changes made.";
            exit;
        ;;
    esac
echo -e $Green"Bye bye"$Noc
exit