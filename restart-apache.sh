#!/usr/bin/env bash
echo "Restarting the PHP/Apache container..."
cd php-apache && docker-compose restart
echo "Restarting the persistent services (Like MySQL) is something you'll rarely do, so we just restart Apache."
echo "You're good to go. Bye."