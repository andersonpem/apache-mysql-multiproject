#!/usr/bin/env bash
# Todo: check for env files before giving the start command
echo "Starting the environments..."
echo "Apache..."
cd php-apache && docker-compose up -d
echo "Shared containers (MySQL and Redis)..."
cd ../services && docker-compose up -d
cd ../
echo "All started."