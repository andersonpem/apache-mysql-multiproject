# Apache MySQL Multiproject

This is a small implementation of project agnostic PHP and MySQL

## Anti-pattern

Yes, I know. But sometimes it's required. Some environments cannot contain Docker in the project.

## How to?

You must have Docker installed, preferrably the latest version, and have permission to run Docker.

Clone this repository, in the same directory you have the projects you want to use it on, but not inside any of them, for example:

```bash
git clone https://gitlab.com/andersonpem/apache-mysql-multiproject.git docker
```

This will create a new folder called docker, which will host this multiproject setup.

Enter the docker folder and execute the script init.sh

```shell
# Creates the shared network for containers and copies the necessary env files
./init.sh
```

All initialization files will be copied.

Go into php-apache and change the .env created there. Set your code location and document root according to your project

Get back to the repo's root directory and start the containers:

```bash
# Starts up the containers. This will take some time on the first run
./start.sh
```

As stated, as Docker has to pull the images, this will take some time on the first run, but after pulled, the initialization of container should take some seconds to start.

## Executing commands (Composer, Artisan or Bin/Console)

We got you covered. Just run:

```shell
./sell.sh
```

And you'll be sent to the Apache container, where you can run Composer and Node and other stuff that you might require. All with Sudo and the same UID as your current logged-in user, so no permission confusion.

**You'll be warned when you leave the container's shell (run an exit command). I got pretty confused when I started working with containers, so I left a friendly reminder that you came back to your actual machine.**

## Aliases (Shorthand commands) - Apache Container

There are a lot of aliases in this container. I'll document them below as the list grows.

| Alias | full command    | Framework specific? | Description                              | Name collisions identified                                                              |
| ----- | --------------- | ------------------- | ---------------------------------------- | --------------------------------------------------------------------------------------- |
| bc    | php bin/console | Symfony             | Shorthand for the Symfony console helper | Yes, there's a linux calculator that has an executable called bc. Replace later to bcs. |
| pa    | php artisan     | Laravel             | Shorthand for Laravel's Artisan          | No                                                                                      |