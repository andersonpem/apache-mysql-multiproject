#!/usr/bin/env bash
clear
source .utils
echo -e $Green"Entering the apache container with the right set of users and permissions..."$Noc
cd php-apache && docker-compose exec  --user=docker php-apache zsh
echo -e $Yellow"========================================================================="
echo -e "  Exited the container's shell. You are now back to your computer's OS."
echo -e "========================================================================="$Noc
