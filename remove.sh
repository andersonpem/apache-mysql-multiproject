
#!/usr/bin/env bash
echo "Remove all containers? No data will be lost from the persistent services like MySQL. \nBut it's good practice if you're not using them. (y/n)"
read yn
    case $yn in
        [Yy]* )
            echo "Shutting down the Apache container..."
                cd php-apache && docker-compose down;
            echo "Shutting down the shared containers..."
                cd ../services && docker-compose down;
        ;;
        [Nn]* )
            echo "Okay. Nothing was done.";
            exit;
        ;;
    esac
echo "All done. Thanks for using."
exit