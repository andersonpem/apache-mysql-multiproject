#!/usr/bin/env bash
source .utils
clear
WORKING_DIRECTORY=$PWD
box_out "Changing the repository in use" "Please provide the folder of the repository you want to use" "Tab completion is available"
cd ../
read -e -p "$(echo -e $Green"Repo Folder> "$Noc)" REPO
if [ -d $REPO ] 
then
    echo -e $Green"Folder was found."$Noc
    read -p "$(echo -e "Enable XDebug $Green(y/n) > $Noc")" XDEBUG
    read -p "$(echo -e "Open the workspace container shell after starting? $Green(y/n) > $Noc")" OPENSHELL
    cd $WORKING_DIRECTORY
    echo -e $Yellow"Changing apache's .env..."$Noc
    .env -f php-apache/.env set CODE_DIRECTORY=../../$REPO
    if [[ $XDEBUG =~ ^[Yy]$ ]]
        then
            ./xdebug.sh on
        else
            ./xdebug.sh off
    fi
    echo -e $Green"Done. Happy coding :)"$Noc
    if [[ $OPENSHELL =~ ^[Yy]$ ]]
    then
        echo -e $Green"Triggering the shell script..."$Noc
        ./shell.sh
    else
        echo -e $Green"Exiting without entering the container's shell."$Noc
        exit 0
    fi
    exit 0
else
    echo -e $Red"Error: Directory $REPO does not exist."
    exit 1
fi