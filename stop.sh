#!/usr/bin/env bash
echo "Stopping all running containers..."
cd php-apache && echo docker-compose stop
cd ../services && docker-compose stop
echo "The project has been stopped."
echo "If instead you want to remove all the containers, use the remove script."