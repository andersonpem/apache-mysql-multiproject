#!/usr/bin/env bash
source .utils
clear
box_out "Setting up the Project's files" "Do you wish to configure this repository? You only have to do this once."
read -p "Proceed [y/n]? " yn
    case $yn in
        [Yy]* )
                echo -e $Green"Creating the shared-containers Docker network..."$Noc;
                docker network create shared-containers;
                if [ $? -eq 0 ]; then
                   echo -e $Green"Network created successfully."$Noc
                else
                    echo -e $Yellow"It seems the network exists already. Exit code: $?"$Noc
                fi
            echo -e $Green"Generating the dotenv files for the Docker projects... \nIf they already exist they won't be overwritten..."$Noc;
            cp -n php-apache/.env.example php-apache/.env && echo -e $Yellow"Apache container .env copied!";
            cp -n services/.env.example services/.env && echo -e "MySQL and Redis containers' .env copied!"$Noc
        ;;
        [Nn]* )
            echo "Ok, then you're already good to go.";
            exit;
        ;;
    esac
echo -e $Green"Finished setting everything up."$Noc
echo -e $Noc"What next? Here goes a list of actions:"
box_out "- Go into php-apache folder and adjust the Docker's .env for your needs" "- Don't forget to set your document root and project code directory!" "- Run the start.sh script to make Docker set all containers up" "- If you need, after the containers initialized," "run the load-sql script to import a sql file to a database." "- Run shell.sh to get into the Apache container and run commands" "- Check your webserver running at http://localhost:8080"
exit