#!/usr/bin/env bash
echo "Changing repository or env config? Let's do it!"
echo "Removing and recreating your apache container... This will take at least 10 seconds..."
cd php-apache && docker-compose down && docker-compose up -d
echo "Remember, Padawan. Containers are efemerous by nature. You should never depend on the state of the container."
echo "You should be able to discard a container and create a new one without loss."